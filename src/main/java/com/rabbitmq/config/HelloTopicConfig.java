package com.rabbitmq.config;

import com.rabbitmq.model.AmqpConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Topic方式
 * 将路由键和某模式进行匹配。此时队列需要绑定要一个模式上。符号“#”匹配一个或多个词，符号“*”匹配不多不少一个词。
 * 因此“audit.#”能够匹配到“audit.irs.corporate”，但是“audit.*” 只会匹配到“audit.irs”。
 * @author ww
 * @date 2020/6/3 上午10:27
 */
@Configuration
public class HelloTopicConfig {
    // 创建队列A
    @Bean
    public Queue queueTopicMessageA() {
        return new Queue(AmqpConstant.QUEUE_TOPIC_MESSAGE_A);
    }
    // 创建队列B
    @Bean
    public Queue queueTopicMessageB() {
        return new Queue(AmqpConstant.QUEUE_TOPIC_MESSAGE_B);
    }
    // 将对列绑定到Topic交换器
    @Bean
    TopicExchange exchange() {
        return new TopicExchange(AmqpConstant.EXCHANGE_TOPIC);
    }
    // 将对列绑定到Topic交换器
    @Bean
    Binding bindingExchangeMessage() {
        return BindingBuilder.bind(queueTopicMessageA()).to(exchange()).with(AmqpConstant.KEY_TOPIC_A);
    }
    // 将对列绑定到Topic交换器 采用#的方式
    @Bean
    Binding bindingExchangeMessages() {
        return BindingBuilder.bind(queueTopicMessageB()).to(exchange()).with(AmqpConstant.KEY_TOPIC);
    }
}
