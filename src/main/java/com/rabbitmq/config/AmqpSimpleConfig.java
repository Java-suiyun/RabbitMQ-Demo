package com.rabbitmq.config;


import com.rabbitmq.model.AmqpConstant;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 简单使用
 */
@Configuration
public class AmqpSimpleConfig {

    @Bean
    public Queue helloQueue() {
        return new Queue(AmqpConstant.SIMPLE_HELLO);
    }
}
