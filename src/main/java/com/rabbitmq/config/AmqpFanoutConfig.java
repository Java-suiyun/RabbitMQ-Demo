package com.rabbitmq.config;

import com.rabbitmq.model.AmqpConstant;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Fanout方式
 * 转发消息到所有绑定队列（和routingKey没有关系）,
 * 即我们熟悉的广播模式或者订阅模式，给Fanout交换机发送消息，绑定了这个交换机的所有队列都收到这个消息。Fanout交换机转发消息是最快的
 * @author ww
 * @date 2020/6/3 下午2:30
 */
@Configuration
public class AmqpFanoutConfig {
    // 创建A队列
    @Bean
    public Queue AMessage() {
        return new Queue(AmqpConstant.QUEUE_FANOUT_MESSAGE_A);
    }
    // 创建B队列
    @Bean
    public Queue BMessage() {
        return new Queue(AmqpConstant.QUEUE_FANOUT_MESSAGE_B);
    }
    // 创建C队列
    @Bean
    public Queue CMessage() {
        return new Queue(AmqpConstant.QUEUE_FANOUT_MESSAGE_C);
    }
    // 创建Fanout交换器
    @Bean
    FanoutExchange fanoutExchange() {
        return new FanoutExchange(AmqpConstant.EXCHANGE_FANOUT);
    }
    // 将A对列绑定到Fanout交换器
    @Bean
    Binding bindingExchangeA() {
        return BindingBuilder.bind(AMessage()).to(fanoutExchange());
    }
    // 将B对列绑定到Fanout交换器
    @Bean
    Binding bindingExchangeB() {
        return BindingBuilder.bind(BMessage()).to(fanoutExchange());
    }
    // 将C对列绑定到Fanout交换器
    @Bean
    Binding bindingExchangeC() {
        return BindingBuilder.bind(CMessage()).to(fanoutExchange());
    }
}
