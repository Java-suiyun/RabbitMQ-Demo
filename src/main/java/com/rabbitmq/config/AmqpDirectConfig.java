package com.rabbitmq.config;

import com.rabbitmq.model.AmqpConstant;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Direct方式
 * 处理路由键。需要将一个队列绑定到交换机上，要求该消息与一个特定的路由键完全匹配
 * @author ww
 * @date 2020/6/3 下午4:09
 */
@Configuration
public class AmqpDirectConfig {

    // 创建队列
    @Bean
    public Queue queueDirectMessage() {
        return new Queue(AmqpConstant.QUEUE_DIRECT_MESSAGE);
    }
    // 将对列绑定到Direct交换器
    @Bean
    DirectExchange exchangeDirect() {
        return new DirectExchange(AmqpConstant.EXCHANGE_DIRECT);
    }

    // 通过路由健把队列绑定到交换机
    @Bean
    Binding bindingExchangeDirect() {
        return BindingBuilder.bind(queueDirectMessage()).to(exchangeDirect()).with(AmqpConstant.KEY_DIRECT);
    }

}
