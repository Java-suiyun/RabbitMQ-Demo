package com.rabbitmq.service;

public interface AmqpService {

    /**
     * 发送消息
     *
     * @param exchange   交换机
     * @param routingKey 路由健
     * @param message    发送的消息
     */
    void send(String exchange, String routingKey, String message);

    /**
     * 发送普通消息；未设置交换机
     * @param queueName   队列名
     * @param message    发送的消息
     */
    void send(String queueName, Object message);

    /**
     * 发送延迟消息
     *
     * @param exchange   交换机
     * @param routingKey 路由健
     * @param message    消息
     */
    void  sendDelay(String exchange, String routingKey, String message);
}
