package com.rabbitmq.service.impl;

import com.rabbitmq.service.AmqpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class AmqpServiceImpl implements AmqpService {

    @Resource
    private AmqpTemplate rabbitTemplate;


    @Override
    public void send(String exchange, String routingKey, String message) {
        log.info("正在发送消息-[exchange]:[{}],[routingKey]:[{}]; [message]: [{}]", exchange, routingKey, message);
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

    @Override
    public void send(String queueName, Object message) {
        log.info("正在发送消息-[queueName]:[{}],; [message]: [{}]", queueName, message);
        rabbitTemplate.convertAndSend(queueName, message);
    }

    @Override
    public void sendDelay(String exchange, String routingKey, String message) {
        log.info("正在发送延时消息-[exchange]:[{}],[routingKey]:[{}]; [message]: [{}]", exchange, routingKey, message);
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

}
