package com.rabbitmq.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ww
 * @date 2020/6/2 下午6:49
 */
@Data
public class User implements Serializable {
    private String username;
    private String password;
}
