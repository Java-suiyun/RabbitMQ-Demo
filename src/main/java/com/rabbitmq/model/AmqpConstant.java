package com.rabbitmq.model;


public class AmqpConstant {

    /**
     * 简单使用
     */
    public static final String SIMPLE_HELLO = "simple_hello";

    /**
     * Direct
     */
    public static final String QUEUE_DIRECT_MESSAGE = "queue_direct_message";
    public static final String EXCHANGE_DIRECT = "exchange_direct";
    public static final String KEY_DIRECT = "key_direct";

    /**
     * Topic
     */
    public static final String QUEUE_TOPIC_MESSAGE_A = "queue_topic_message_A";
    public static final String QUEUE_TOPIC_MESSAGE_B = "queue_topic_message_B";
    public static final String EXCHANGE_TOPIC = "exchange_topic";
    public static final String KEY_TOPIC_A = "key.topic.message.A";
    public static final String KEY_TOPIC_B = "key.topic.message.B";
    //.为分隔符匹配
    public static final String KEY_TOPIC = "key.topic.#";

    /**
     * Fanout
     */
    public static final String QUEUE_FANOUT_MESSAGE_A = "queue_fanout_message_a";
    public static final String QUEUE_FANOUT_MESSAGE_B = "queue_fanout_message_b";
    public static final String QUEUE_FANOUT_MESSAGE_C = "queue_fanout_message_c";
    public static final String EXCHANGE_FANOUT = "fanout_topic";

    /**
     * 延迟队列
     */
    //延迟队列 TTL 名称
    public static final String QUEUE_DELAY_CANCEL_ORDER= "queue_delay_cancel_order";
    // DLX，dead letter发送到的 exchange 延时消息就是发送到该交换机的
    public static final String EXCHANGE_DELAY_CANCEL_ORDER = "exchange_delay_cancel_order";
    //routing key 名称
    public static final String KEY_DELAY_CANCEL_ORDER = "key_delay_cancel_order";

    public static final String QUEUE_CANCEL_ORDER= "queue_cancel_order";
    public static final String EXCHANGE_CANCEL_ORDER = "exchange_cancel_order";
    public static final String KEY_CANCEL_ORDER = "key_cancel_order";

    public static final Long TTL_DELAY_CANCEL_ORDER = 60 * 1000L;


}
