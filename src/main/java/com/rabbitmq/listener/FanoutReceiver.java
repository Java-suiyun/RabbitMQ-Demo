package com.rabbitmq.listener;

import com.rabbitmq.model.AmqpConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author ww
 * @date 2020/6/3 下午2:48
 */
@Slf4j
@Component
public class FanoutReceiver {
    @RabbitListener(queues = AmqpConstant.QUEUE_FANOUT_MESSAGE_A)
    public void processA(String message) {
        log.info("Fanout ReceiverA  : {}", message);
    }

    @RabbitListener(queues = AmqpConstant.QUEUE_FANOUT_MESSAGE_B)
    public void processB(String message) {
        log.info("Fanout ReceiverB  : {}" ,message);
    }

    @RabbitListener(queues = AmqpConstant.QUEUE_FANOUT_MESSAGE_C)
    public void processC(String message) {
        log.info("Fanout ReceiverC  : {}" ,message);
    }
}
