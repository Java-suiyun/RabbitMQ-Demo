package com.rabbitmq.listener;

import com.rabbitmq.model.AmqpConstant;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author ww
 * @date 2020/6/3 下午4:59
 */
@Component
public class DirectReceiver {

    @RabbitListener(queues = AmqpConstant.QUEUE_DIRECT_MESSAGE)
    public void processA(String message) {
        System.out.println("Direct ReceiverA  : " + message);
    }
}
