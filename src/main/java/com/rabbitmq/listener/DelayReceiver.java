package com.rabbitmq.listener;

import com.rabbitmq.model.AmqpConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author ww
 * @date 2020/6/3 下午3:59
 */
@Slf4j
@Component
public class DelayReceiver {

    @RabbitListener(queues = {AmqpConstant.QUEUE_CANCEL_ORDER})
    public void orderDelayQueue(String orderNo) {
        log.info("【orderDelayQueue 监听的消息】 - 【消费时间】 - [{}]- 【订单内容】 - [{}]",  new Date(), orderNo);
    }
}
