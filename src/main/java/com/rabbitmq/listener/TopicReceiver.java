package com.rabbitmq.listener;

import com.rabbitmq.model.AmqpConstant;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author ww
 * @date 2020/6/3 上午11:15
 */
@Component
public class TopicReceiver {

    @RabbitListener(queues = AmqpConstant.QUEUE_TOPIC_MESSAGE_A)
    public void processA(String message) {
        System.out.println("Topic ReceiverA  : " + message);
    }

    @RabbitListener(queues = AmqpConstant.QUEUE_TOPIC_MESSAGE_B)
    public void processB(String message) {
        System.out.println("Topic ReceiverB  : " + message);
    }
}
