package com.rabbitmq.listener;

import com.rabbitmq.model.AmqpConstant;
import com.rabbitmq.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author ww
 * @date 2020/6/2 下午6:22
 * @RabbitListener 可以标注在类上面，需配合 @RabbitHandler 注解一起使用
 * @RabbitListener 标注在类上面表示当有收到消息的时候，就交给 @RabbitHandler 的方法处理，具体使用哪个方法处理，根据 MessageConverter 转换后的参数类型
 *
 */
@Slf4j
@Component
@RabbitListener(queues = AmqpConstant.SIMPLE_HELLO)
public class HelloSimpleReceiver {

    @RabbitHandler
    public void process(String hello) {
       log.info("Receiver Simple : {}" ,hello);
    }

    @RabbitHandler
    public void process(User user) {
       log.info("Receiver Simple User  : {}",user);

    }
}
