package com.rabbitmq.controller;

import com.rabbitmq.model.AmqpConstant;
import com.rabbitmq.service.AmqpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 * @date 2020/6/3 上午11:20
 */
@Slf4j
@RestController
@RequestMapping("/topic")
public class TopicDemoController {

    @Autowired
    private AmqpService amqpService;

    /**
     * 接收两条
     * 匹配HelloTopicConfig.keyA 和kye.#
     * @return
     */
    @GetMapping("/test/a")
    public Boolean sendTopicDemoA(){
        String context = "hi, i am message A";
        log.info("Sender A: {}",context);
        this.amqpService.send(AmqpConstant.EXCHANGE_TOPIC, AmqpConstant.KEY_TOPIC_A, context);

        return Boolean.TRUE;
    }

    /**
     * 接收一条
     * 匹配HelloTopicConfig.key.#
     * @return
     */
    @GetMapping("/test/b")
    public Boolean sendTopicDemoB(){
        String context = "hi, i am messages B";
        log.info("Sender B: {}",context);
        this.amqpService.send(AmqpConstant.EXCHANGE_TOPIC, AmqpConstant.KEY_TOPIC_B, context);

        return Boolean.TRUE;
    }
}
