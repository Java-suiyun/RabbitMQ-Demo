package com.rabbitmq.controller;

import com.rabbitmq.model.AmqpConstant;
import com.rabbitmq.service.AmqpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 * @date 2020/6/3 下午2:45
 */
@Slf4j
@RequestMapping("/fanout")
@RestController
public class FanoutDemoController {

    @Autowired
    private AmqpService amqpService;

    @GetMapping("/test")
    public Boolean fanoutDemo(){
        String context = "hi, fanout msg ";
        log.info("fanout Sender : {}",context);
        this.amqpService.send(AmqpConstant.EXCHANGE_FANOUT,"", context);

        return Boolean.TRUE;
    }
}
