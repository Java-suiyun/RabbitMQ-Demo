package com.rabbitmq.controller;

import com.rabbitmq.model.AmqpConstant;
import com.rabbitmq.service.AmqpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 * @date 2020/6/3 上午11:20
 */
@Slf4j
@RestController
@RequestMapping("/direct")
public class DirectDemoController {

    @Autowired
    private AmqpService amqpService;

    @GetMapping("/test")
    public Boolean sendDirectDemoA(){
        String context = "hi, i am message";
        log.info("Sender: {}",context);
        this.amqpService.send(AmqpConstant.EXCHANGE_DIRECT, AmqpConstant.KEY_DIRECT, context);

        return Boolean.TRUE;
    }

}
