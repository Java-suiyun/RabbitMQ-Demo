package com.rabbitmq.controller;

import com.rabbitmq.model.AmqpConstant;
import com.rabbitmq.model.User;
import com.rabbitmq.service.AmqpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ww
 * @date 2020/6/2 下午6:27
 */
@RequestMapping("/simple/demo")
@RestController
public class HellpSimpleDemoController {

    @Autowired
    private AmqpService amqpService;

    @GetMapping("/hello")
    public Boolean sendHellSimple(){
        this.amqpService.send(AmqpConstant.SIMPLE_HELLO,"simpleHello");
        return Boolean.TRUE;
    }

    @GetMapping("/hello/user")
    public Boolean sendHellUserSimple(){
        User user = new User();
        user.setUsername("admin");
        user.setPassword("1234");
        this.amqpService.send(AmqpConstant.SIMPLE_HELLO,user);
        return Boolean.TRUE;
    }
}
