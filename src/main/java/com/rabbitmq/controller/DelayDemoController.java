package com.rabbitmq.controller;

import com.rabbitmq.model.AmqpConstant;
import com.rabbitmq.service.AmqpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * @author ww
 * @date 2020/6/3 下午3:55
 */
@Slf4j
@RequestMapping("/delay")
@RestController
public class DelayDemoController {

    @Autowired
    private AmqpService amqpService;

    @GetMapping("/test")
    public Boolean sendDelay() {
        log.info("【订单生成时间】" + new Date().toString() +"【1分钟后检查订单是否已经支付】");
        this.amqpService.sendDelay(AmqpConstant.EXCHANGE_DELAY_CANCEL_ORDER,AmqpConstant.KEY_DELAY_CANCEL_ORDER, UUID.randomUUID().toString());
        return Boolean.TRUE;
    }
}
